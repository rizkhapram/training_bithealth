﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace TokoSaya.DataModels
{
    public partial class DBfirst_TokoSayaContext : DbContext
    {
        public DBfirst_TokoSayaContext()
        {
        }

        public DBfirst_TokoSayaContext(DbContextOptions<DBfirst_TokoSayaContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TblCategoies> TblCategoies { get; set; }
        public virtual DbSet<TblCustomers> TblCustomers { get; set; }
        public virtual DbSet<TblOrderDetails> TblOrderDetails { get; set; }
        public virtual DbSet<TblOrderHeaders> TblOrderHeaders { get; set; }
        public virtual DbSet<TblProducts> TblProducts { get; set; }
        public virtual DbSet<TblVariants> TblVariants { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=localhost;Initial Catalog=DBfirst_TokoSaya;user id=sa;Password=12345678");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TblCategoies>(entity =>
            {
                entity.HasKey(e => e.IdCategory);

                entity.ToTable("tbl_categoies");

                entity.Property(e => e.IdCategory).HasColumnName("id_category");

                entity.Property(e => e.CreateBy).HasColumnName("create_by");

                entity.Property(e => e.CreateDate)
                    .HasColumnName("create_date")
                    .HasColumnType("date");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .IsUnicode(false);

                entity.Property(e => e.IsDelete).HasColumnName("is_delete");

                entity.Property(e => e.NameCategory)
                    .IsRequired()
                    .HasColumnName("name_category")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy).HasColumnName("update_by");

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("date");
            });

            modelBuilder.Entity<TblCustomers>(entity =>
            {
                entity.HasKey(e => e.IdCustomer)
                    .HasName("PK__tbl_cust__8CC9BA46FFF3861A");

                entity.ToTable("tbl_customers");

                entity.Property(e => e.IdCustomer).HasColumnName("id_customer");

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasColumnName("address");

                entity.Property(e => e.CreateBy).HasColumnName("create_by");

                entity.Property(e => e.CreateDate).HasColumnName("create_date");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasMaxLength(50);

                entity.Property(e => e.IdRole).HasColumnName("id_role");

                entity.Property(e => e.IsDelete).HasColumnName("is_delete");

                entity.Property(e => e.NameCustomer)
                    .IsRequired()
                    .HasColumnName("name_customer")
                    .HasMaxLength(50);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnName("password")
                    .HasMaxLength(100);

                entity.Property(e => e.Phone)
                    .IsRequired()
                    .HasColumnName("phone")
                    .HasMaxLength(15);

                entity.Property(e => e.UpdateBy).HasColumnName("update_by");

                entity.Property(e => e.UpdateDate).HasColumnName("update_date");
            });

            modelBuilder.Entity<TblOrderDetails>(entity =>
            {
                entity.HasKey(e => e.IdDetail)
                    .HasName("PK__order_de__EA833808857E1426");

                entity.ToTable("tbl_order_details");

                entity.Property(e => e.IdDetail).HasColumnName("id_detail");

                entity.Property(e => e.CreateBy).HasColumnName("create_by");

                entity.Property(e => e.CreateDate)
                    .HasColumnName("create_date")
                    .HasColumnType("date");

                entity.Property(e => e.IdHeader).HasColumnName("id_header");

                entity.Property(e => e.IdProduct).HasColumnName("id_product");

                entity.Property(e => e.IsDelete).HasColumnName("is_delete");

                entity.Property(e => e.Quantity).HasColumnName("quantity");

                entity.Property(e => e.SumPrice)
                    .HasColumnName("sum_price")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.UpdateBy).HasColumnName("update_by");

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("date");

                entity.HasOne(d => d.IdHeaderNavigation)
                    .WithMany(p => p.TblOrderDetails)
                    .HasForeignKey(d => d.IdHeader)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tbl_order_details_tbl_order_headers");

                entity.HasOne(d => d.IdProductNavigation)
                    .WithMany(p => p.TblOrderDetails)
                    .HasForeignKey(d => d.IdProduct)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tbl_order_details_tbl_products");
            });

            modelBuilder.Entity<TblOrderHeaders>(entity =>
            {
                entity.HasKey(e => e.IdHeader)
                    .HasName("PK__order_he__75F6DF6BDCD3D568");

                entity.ToTable("tbl_order_headers");

                entity.Property(e => e.IdHeader).HasColumnName("id_header");

                entity.Property(e => e.Amount)
                    .HasColumnName("amount")
                    .HasColumnType("decimal(18, 2)");

                entity.Property(e => e.CodeTransaction)
                    .IsRequired()
                    .HasColumnName("code_transaction")
                    .HasMaxLength(20);

                entity.Property(e => e.CreateBy).HasColumnName("create_by");

                entity.Property(e => e.CreateDate)
                    .HasColumnName("create_date")
                    .HasColumnType("date");

                entity.Property(e => e.IdCustomer).HasColumnName("id_customer");

                entity.Property(e => e.IsCheckout).HasColumnName("is_checkout");

                entity.Property(e => e.IsDelete).HasColumnName("is_delete");

                entity.Property(e => e.UpdateBy).HasColumnName("update_by");

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("date");
            });

            modelBuilder.Entity<TblProducts>(entity =>
            {
                entity.HasKey(e => e.IdProduct);

                entity.ToTable("tbl_products");

                entity.Property(e => e.IdProduct).HasColumnName("id_product");

                entity.Property(e => e.CreateBy).HasColumnName("create_by");

                entity.Property(e => e.CreateDate)
                    .HasColumnName("create_date")
                    .HasColumnType("date");

                entity.Property(e => e.IdVariant).HasColumnName("id_variant");

                entity.Property(e => e.Image)
                    .HasColumnName("image")
                    .IsUnicode(false);

                entity.Property(e => e.IsDelete).HasColumnName("is_delete");

                entity.Property(e => e.NameProduct)
                    .IsRequired()
                    .HasColumnName("name_product")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Stock).HasColumnName("stock");

                entity.Property(e => e.UpdateBy).HasColumnName("update_by");

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("date");

                entity.HasOne(d => d.IdVariantNavigation)
                    .WithMany(p => p.TblProducts)
                    .HasForeignKey(d => d.IdVariant)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tbl_products_tbl_variants");
            });

            modelBuilder.Entity<TblVariants>(entity =>
            {
                entity.HasKey(e => e.IdVariant);

                entity.ToTable("tbl_variants");

                entity.Property(e => e.IdVariant).HasColumnName("id_variant");

                entity.Property(e => e.CreateBy).HasColumnName("create_by");

                entity.Property(e => e.CreateDate).HasColumnName("create_date");

                entity.Property(e => e.Description).HasColumnName("description");

                entity.Property(e => e.IdCategory).HasColumnName("id_category");

                entity.Property(e => e.IsDelete).HasColumnName("is_delete");

                entity.Property(e => e.NameVariant)
                    .IsRequired()
                    .HasColumnName("name_variant")
                    .HasMaxLength(100);

                entity.Property(e => e.UpdateBy).HasColumnName("update_by");

                entity.Property(e => e.UpdateDate).HasColumnName("update_date");

                entity.HasOne(d => d.IdCategoryNavigation)
                    .WithMany(p => p.TblVariants)
                    .HasForeignKey(d => d.IdCategory);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
