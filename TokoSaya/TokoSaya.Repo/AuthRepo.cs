﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoSaya.DataModels;
using TokoSaya.ViewModels;


namespace TokoSaya.Repo
{
    public class AuthRepo
    {
        public static IMapper GetMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<TblCustomers, VMCustomers>();
                cfg.CreateMap<VMCustomers, TblCustomers>();
            });

            IMapper mapper = config.CreateMapper();

            return mapper;
        }
        public static VMRespon LoginCheck(DBfirst_TokoSayaContext db, VMCustomers dataParam)
        {
            VMRespon res = new VMRespon();
            TblCustomers cs = db.TblCustomers.Where
                                (a => a.Email == dataParam.Email 
                                   && a.Password == dataParam.Password).FirstOrDefault();

            if(cs != null)
            {
                res.vmCustomer = GetMapper().Map<VMCustomers>(cs);
                res.isValid = true;
                res.Message = "/Home/Index";
                return res;
            }
            else
            {
                res.isValid = false;
                res.Message = "Email and password not found";
                return res;
            }
        }

        public static VMRespon EmailCheck(DBfirst_TokoSayaContext db, string email)
        {
            VMRespon res = new VMRespon();
            TblCustomers cs = db.TblCustomers.Where
                                (a => a.Email == email).FirstOrDefault();

            if (cs != null)
            {
                res.isValid = true;
                res.Message = "Email is already";
                return res;
            }
            else
            {
                res.isValid = false;
                res.Message = "";
                return res;
            }
        }

        public static VMRespon CreateCustomer(DBfirst_TokoSayaContext db, VMCustomers dataParam)
        {

            VMRespon respon = new VMRespon();
            TblCustomers data = GetMapper().Map<TblCustomers>(dataParam);


            try
            {
                data.CreateBy = 1;
                data.CreateDate = DateTime.Now;

                db.Add(data);
                db.SaveChanges();
                respon.isValid = true;
                respon.Message = "successfully save data";

                return respon;
            }
            catch (Exception e)
            {
                respon.isValid = false;
                respon.Message = e.Message;
                return respon;
                throw;
            }

        }
    }
}
