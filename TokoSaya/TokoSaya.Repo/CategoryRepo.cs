﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoSaya.DataModels;
using TokoSaya.ViewModels;

namespace TokoSaya.Repo
{
    public class CategoryRepo
    {
        public static IMapper GetMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<TblCategoies, VMCategory>();
                cfg.CreateMap<VMCategory, TblCategoies>();
            });

            IMapper mapper = config.CreateMapper();

            return mapper;
        }

        public static async Task<List<VMCategory>> AllData(DBfirst_TokoSayaContext db)
        {
            List<TblCategoies> dataFromDb =  await db.TblCategoies.
                                                Where(a => a.IsDelete == false).ToListAsync();

            List<VMCategory> data = GetMapper().Map<List<VMCategory>>(dataFromDb);

            return data;
        }

        public static VMRespon Create(DBfirst_TokoSayaContext db, VMCategory dataParam)
        {

            TblCategoies data = GetMapper().Map<TblCategoies>(dataParam);
            VMRespon respon = new VMRespon();

            try
            {
                //data.CreateBy = 1;
                data.CreateDate = DateTime.Now;

                db.Add(data);
                db.SaveChanges();
                respon.isValid = true;
                respon.Message = "successfully save data";

                return respon;
            }
            catch (Exception e)
            {
                respon.isValid = false;
                respon.Message = e.Message;
                return respon;
                throw;
            }

        }

        public static VMCategory GetDataById(DBfirst_TokoSayaContext db, int id)
        {
            TblCategoies dataDb = GetDataModelById(db, id);

            VMCategory data = GetMapper().Map<VMCategory>(dataDb);

            return data;
        }

        public static TblCategoies GetDataModelById(DBfirst_TokoSayaContext db, int id)
        {
            TblCategoies dataDb = db.TblCategoies
                                    .Where(a => a.IdCategory == id).FirstOrDefault();

            
            return dataDb;
        }

        public static VMRespon Edit(DBfirst_TokoSayaContext db, VMCategory dataParam)
        {
            TblCategoies dataDb = GetDataModelById(db, dataParam.IdCategory);
            VMRespon respon = new VMRespon();
            try
            {
                dataDb.NameCategory = dataParam.NameCategory;
                dataDb.Description = dataParam.Description;
                dataDb.UpdateBy = 1;
                dataDb.UpdateDate = DateTime.Now;

                db.Update(dataDb);
                db.SaveChanges();

                respon.isValid = true;
                respon.Message = "successfully save data";

                return respon;
            }
            catch (Exception e)
            {
                respon.isValid = true;
                respon.Message = e.Message;

                return respon;
                throw;
            }
        }

        public static VMRespon Delete(DBfirst_TokoSayaContext db, VMCategory dataParam)
        {
            TblCategoies dataDb = GetDataModelById(db, dataParam.IdCategory);
            VMRespon respon = new VMRespon();

            try
            {
                dataDb.IsDelete = true;
                dataDb.UpdateBy = 1;
                dataDb.UpdateDate = DateTime.Now;

                db.Update(dataDb);
                db.SaveChanges();

                respon.isValid = true;
                respon.Message = "successfully update data";

                return respon;

            }
            catch (Exception e)
            {
                respon.isValid = true;
                respon.Message = e.Message;

                return respon;
                throw;
            }
        }

    }
}
