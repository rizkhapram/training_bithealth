﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoSaya.DataModels;
using TokoSaya.ViewModels;

namespace TokoSaya.Repo
{
    public class OrderRepo
    {
        public static IMapper GetMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<TblOrderHeaders, VMOrderHeaders>();
                cfg.CreateMap<VMOrderHeaders, VMOrderHeaders>();

                cfg.CreateMap<TblOrderDetails, VMOrderDetails>();
                cfg.CreateMap<VMOrderDetails, TblOrderDetails>();
            });

            IMapper mapper = config.CreateMapper();
            return mapper;
        }

        public static List<VMOrderHeaders> DataHeader(DBfirst_TokoSayaContext db, int? idCust)
        {
            List<TblOrderHeaders> dataDb = db.TblOrderHeaders.Where(a=>a.IdCustomer == idCust).ToList();
            List<VMOrderHeaders> data = GetMapper().Map<List<VMOrderHeaders>>(dataDb);
            return data;
        }

        public static List<VMOrderHeaders> DataDetail(DBfirst_TokoSayaContext db, int idHeader)
        {
            List<VMOrderHeaders> data = (from head in db.TblOrderHeaders
                                         join detail in db.TblOrderDetails
                                            on head.IdHeader equals detail.IdHeader
                                         join prod in db.TblProducts
                                             on detail.IdProduct equals prod.IdProduct
                                         join var in db.TblVariants
                                             on prod.IdVariant equals var.IdVariant
                                         join cat in db.TblCategoies
                                             on var.IdCategory equals cat.IdCategory
                                         where head.IdHeader == idHeader
                                         select new VMOrderHeaders
                                         {
                                             IdHeader = head.IdHeader,
                                             CodeTransaction = head.CodeTransaction,
                                             IdCustomer = head.IdCustomer,
                                             Amount = head.Amount,
                                             CreateDate = head.CreateDate,
                                             IdDetailNavigation = detail,
                                             IdProductNavigation = prod

                                         }).ToList();
            return data;
        }

        public static VMRespon Order(DBfirst_TokoSayaContext db, List<VMProducts> list, int TotalProduct,
                                    int Estimate, int? IdCustomer)
        {
            VMRespon respon = new VMRespon();
            TblOrderHeaders head = new TblOrderHeaders();
            try
            {
                head.CodeTransaction = GenerateCodeTransaction(db);
                head.IdCustomer = Convert.ToInt32(IdCustomer);
                head.IsCheckout = false;
                head.Amount = Estimate;
                head.CreateBy = IdCustomer;
                head.CreateDate = DateTime.Now;

                db.Add(head);
                db.SaveChanges();

                foreach (VMProducts item in list)
                {
                    TblOrderDetails detail = new TblOrderDetails();

                    detail.IdHeader = head.IdHeader;
                    detail.IdProduct = item.IdProduct;
                    detail.Quantity = item.Qty;
                    detail.SumPrice = item.SumPrice;
                    detail.CreateBy = 1;
                    detail.CreateDate = DateTime.Now;

                    db.Add(detail);
                    db.SaveChanges();

                    TblProducts prod = db.TblProducts.Where(a => a.IdProduct == item.IdProduct).FirstOrDefault();
                    if (prod != null)
                    {
                        prod.Stock = prod.Stock - detail.Quantity;
                        prod.UpdateBy = 1;
                        prod.UpdateDate = DateTime.Now;

                        db.Update(prod);
                        db.SaveChanges();
                    }
                }

                respon.isValid = true;
                respon.Message = "Thank you for order";
                return respon;
            }
            catch (Exception e)
            {
                respon.isValid = false;
                respon.Message = "Unsuccess order " + e.Message;
                return respon;
                throw;
            }
        }

        public static string GenerateCodeTransaction(DBfirst_TokoSayaContext db)
        {
            DateTime dtnow = DateTime.Now;
            int d = dtnow.Day;
            int m = dtnow.Month;
            int y = dtnow.Year;
            string codetrx = $"TS-{d}{m}{y}-0001";

            //TS-26042022-0001
            TblOrderHeaders data = db.TblOrderHeaders.OrderByDescending(a => a.CodeTransaction)
                                                         .FirstOrDefault();
            if (data != null)
            {
                codetrx = data.CodeTransaction;
                string substrCode = data.CodeTransaction.Substring(codetrx.Length - 4, 4);
                int lastInt = int.Parse(substrCode) + 1;
                string joinStr = "0000" + lastInt.ToString();

                codetrx = $"TS-{d}{m}{y}-{joinStr.Substring(joinStr.Length - 4, 4)}";

            }
            return codetrx;

        }
    }
}
