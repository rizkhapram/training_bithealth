﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TokoSaya.DataModels;
using TokoSaya.ViewModels;

namespace TokoSaya.Repo
{
    public class ProductRepo
    {
        public static IMapper GetMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<TblProducts, VMProducts>();
                cfg.CreateMap<VMProducts, TblProducts>();
            });

            IMapper mapper = config.CreateMapper();

            return mapper;
        }

        public static List<VMProducts> AllData(DBfirst_TokoSayaContext db)
        {
            List<VMProducts> dataDb = (from prod in db.TblProducts
                                       join var in db.TblVariants
                                           on prod.IdVariant equals var.IdVariant
                                       join cat in db.TblCategoies
                                           on var.IdCategory equals cat.IdCategory
                                       where prod.IsDelete == false && var.IsDelete == false
                                               && cat.IsDelete == false
                                       select new VMProducts
                                       {
                                           IdProduct = prod.IdProduct,
                                           Stock = prod.Stock,
                                           NameProduct = prod.NameProduct,
                                           Price = prod.Price,
                                           Image = prod.Image,
                                           IdVariantNavigation = var,
                                           IdCategoryNavigation = cat
                                       }).ToList();



            return dataDb;
        }

        public static VMRespon Create(DBfirst_TokoSayaContext db, VMProducts dataParam)
        {
            TblProducts data = GetMapper().Map<TblProducts>(dataParam);
            VMRespon respon = new VMRespon();

            try
            {
                data.CreateBy = 1;
                data.CreateDate = DateTime.Now;

                db.Add(data);
                db.SaveChanges();
                respon.isValid = true;
                respon.Message = "successfully save data";

                return respon;
            }
            catch (Exception e)
            {
                respon.isValid = false;
                respon.Message = e.Message;
                return respon;
                throw;
            }

        }

        public static VMProducts GetDataById(DBfirst_TokoSayaContext db, int id)
        {
            VMProducts data = (from prod in db.TblProducts
                               join var in db.TblVariants
                                   on prod.IdVariant equals var.IdVariant
                               join cat in db.TblCategoies
                                   on var.IdCategory equals cat.IdCategory
                               where prod.IdProduct == id
                               select new VMProducts
                               {
                                   IdProduct = prod.IdProduct,
                                   Stock = prod.Stock,
                                   NameProduct = prod.NameProduct,
                                   Price = prod.Price,
                                   IdVariant = var.IdVariant,
                                   IdCategory = cat.IdCategory,
                                   Image = prod.Image,
                                   IdCategoryNavigation = cat,
                                   IdVariantNavigation = var
                               }).FirstOrDefault();
            return data;
        }

        public static VMRespon Edit(DBfirst_TokoSayaContext db, VMProducts dataParam)
        {
            TblProducts dataDb = db.TblProducts
                                .Where(a => a.IdProduct == dataParam.IdProduct).FirstOrDefault();
            VMRespon respon = new VMRespon();
            try
            {
                dataDb.NameProduct = dataParam.NameProduct;
                dataDb.Stock = dataParam.Stock;
                dataDb.Price = dataParam.Price;
                dataDb.IdVariant = dataParam.IdVariant;
                dataDb.Image = dataParam.Image != null ? dataParam.Image : dataDb.Image;
                dataDb.UpdateBy = 1;
                dataDb.UpdateDate = DateTime.Now;

                db.Update(dataDb);
                db.SaveChanges();

                respon.isValid = true;
                respon.Message = "successfully save data";

                return respon;
            }
            catch (Exception e)
            {
                respon.isValid = true;
                respon.Message = e.Message;

                return respon;
                throw;
            }
        }

        public static VMRespon Delete(DBfirst_TokoSayaContext db, VMProducts dataParam)
        {
            TblProducts dataDb = db.TblProducts
                                .Where(a => a.IdProduct == dataParam.IdProduct).FirstOrDefault();
            VMRespon respon = new VMRespon();

            try
            {
                dataDb.IsDelete = true;
                dataDb.UpdateBy = 1;
                dataDb.UpdateDate = DateTime.Now;

                db.Update(dataDb);
                db.SaveChanges();

                respon.isValid = true;
                respon.Message = "successfully update data";

                return respon;

            }
            catch (Exception e)
            {
                respon.isValid = true;
                respon.Message = e.Message;

                return respon;
                throw;
            }
        }
    }
}
