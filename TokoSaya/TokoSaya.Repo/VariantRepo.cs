﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TokoSaya.DataModels;
using TokoSaya.ViewModels;

namespace TokoSaya.Repo
{


    public class VariantRepo
    {
        public static IMapper GetMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<TblVariants, VMVariants>();
                cfg.CreateMap<VMVariants, TblVariants>();
            });

            IMapper mapper = config.CreateMapper();

            return mapper;
        }

        public static List<VMVariants> AllData(DBfirst_TokoSayaContext db)
        {
            List<TblVariants> dataDb = (from var in db.TblVariants
                                        join cat in db.TblCategoies
                                        on var.IdCategory equals cat.IdCategory
                                        where var.IsDelete == false
                                                && cat.IsDelete == false
                                        select new TblVariants
                                        {
                                            IdVariant = var.IdVariant,
                                            NameVariant = var.NameVariant,
                                            Description = var.Description,
                                            IdCategory = var.IdCategory,
                                            IdCategoryNavigation = cat
                                        }).ToList();


            List<VMVariants> data = GetMapper().Map<List<VMVariants>>(dataDb);

            return data;
        }

        public static VMRespon Create(DBfirst_TokoSayaContext db, VMVariants dataParam)
        {
            TblVariants data = GetMapper().Map<TblVariants>(dataParam);
            VMRespon respon = new VMRespon();

            try
            {
                data.CreateBy = 1;
                data.CreateDate = DateTime.Now;

                db.Add(data);
                db.SaveChanges();
                respon.isValid = true;
                respon.Message = "successfully save data";

                return respon;
            }
            catch (Exception e)
            {
                respon.isValid = false;
                respon.Message = e.Message;
                return respon;
                throw;
            }

        }

        public static List<VMVariants> DataVariantByIdCategory(DBfirst_TokoSayaContext db, int id)
        {
            List<TblVariants> listDb = db.TblVariants.Where(a => a.IdCategory == id
                                                            && a.IsDelete == false).ToList();
            List<VMVariants> list = GetMapper().Map<List<VMVariants>>(listDb);

            return list;

        }
        public static VMVariants GetDataById(DBfirst_TokoSayaContext db, int id)
        {
            TblVariants dataDb = GetDataModelById(db, id);

            VMVariants data = GetMapper().Map<VMVariants>(dataDb);

            return data;
        }

        public static TblVariants GetDataModelById(DBfirst_TokoSayaContext db, int id)
        {
            TblVariants dataDb = (from var in db.TblVariants
                                  join cat in db.TblCategoies
                                   on var.IdCategory equals cat.IdCategory
                                  where var.IdVariant == id
                                  select new TblVariants
                                  {
                                      IdVariant = var.IdVariant,
                                      NameVariant = var.NameVariant,
                                      Description = var.Description,
                                      IdCategory = var.IdCategory,
                                      IdCategoryNavigation = cat
                                  }).FirstOrDefault();


            return dataDb;
        }

        public static VMRespon Edit(DBfirst_TokoSayaContext db, VMVariants dataParam)
        {
            TblVariants dataDb = db.TblVariants
                                .Where(a => a.IdVariant == dataParam.IdVariant).FirstOrDefault();
            VMRespon respon = new VMRespon();
            try
            {
                dataDb.NameVariant = dataParam.NameVariant;
                dataDb.Description = dataParam.Description;
                dataDb.IdCategory = dataParam.IdCategory;
                dataDb.UpdateBy = 1;
                dataDb.UpdateDate = DateTime.Now;

                db.Update(dataDb);
                db.SaveChanges();

                respon.isValid = true;
                respon.Message = "successfully save data";

                return respon;
            }
            catch (Exception e)
            {
                respon.isValid = true;
                respon.Message = e.Message;

                return respon;
                throw;
            }
        }

        public static VMRespon Delete(DBfirst_TokoSayaContext db, VMVariants dataParam)
        {
            TblVariants dataDb = GetDataModelById(db, dataParam.IdVariant);
            VMRespon respon = new VMRespon();

            try
            {
                dataDb.IsDelete = true;
                dataDb.UpdateBy = 1;
                dataDb.UpdateDate = DateTime.Now;

                db.Update(dataDb);
                db.SaveChanges();

                respon.isValid = true;
                respon.Message = "successfully update data";

                return respon;

            }
            catch (Exception e)
            {
                respon.isValid = true;
                respon.Message = e.Message;

                return respon;
                throw;
            }
        }
    }
}
