﻿using System;
using System.Collections.Generic;
using System.Text;
using TokoSaya.DataModels;

namespace TokoSaya.ViewModels
{
    public class VMOrderDetails
    {
        public int IdDetail { get; set; }
        public int IdHeader { get; set; }
        public int IdProduct { get; set; }
        public int Quantity { get; set; }
        public decimal SumPrice { get; set; }

        public bool IsDelete { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }

        public virtual TblOrderHeaders IdHeaderNavigation { get; set; }
        public virtual TblProducts IdProductNavigation { get; set; }
    }
}
