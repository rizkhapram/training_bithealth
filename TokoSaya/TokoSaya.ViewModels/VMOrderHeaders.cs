﻿using System;
using System.Collections.Generic;
using System.Text;
using TokoSaya.DataModels;

namespace TokoSaya.ViewModels
{
    public class VMOrderHeaders
    {

        public int IdHeader { get; set; }
        public string CodeTransaction { get; set; }
        public int IdCustomer { get; set; }
        public bool IsCheckout { get; set; }
        public decimal Amount { get; set; }

        public bool IsDelete { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }

        public virtual ICollection<TblOrderDetails> TblOrderDetails { get; set; }

        public virtual TblOrderDetails IdDetailNavigation { get; set; }
        public virtual TblProducts IdProductNavigation { get; set; }
    }
}
