﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using TokoSaya.DataModels;

namespace TokoSaya.ViewModels
{
    public class VMProducts
    {
        public int IdProduct { get; set; }
        public string NameProduct { get; set; }
        public decimal Price { get; set; }
        public int Stock { get; set; }
        public int Qty { get; set; }
        public int SumPrice { get; set; }

        public int IdVariant { get; set; }
        public int IdCategory { get; set; }
        public string Image { get; set; }
        public bool IsDelete { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }

        public IFormFile ImageFile { get; set; }

        public virtual TblVariants IdVariantNavigation { get; set; }
        public virtual TblCategoies IdCategoryNavigation { get; set; }
    }
}
