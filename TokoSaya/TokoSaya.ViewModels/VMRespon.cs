﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TokoSaya.ViewModels
{
    public class VMRespon
    {
        public bool isValid { get; set; }
        public string Message { get; set; }

        public virtual VMCustomers vmCustomer { get; set; }
    }
}
