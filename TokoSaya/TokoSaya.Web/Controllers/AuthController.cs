﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoSaya.DataModels;
using TokoSaya.ViewModels;
using TokoSaya.Web.AddOns;

namespace TokoSaya.Web.Controllers
{
    public class AuthController : Controller
    {
        private readonly DBfirst_TokoSayaContext db;
        public AuthController(DBfirst_TokoSayaContext _db)
        {
            this.db = _db;
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(VMCustomers dataParam)
        {
            VMRespon res = Repo.AuthRepo.LoginCheck(db, dataParam);
            if(res.vmCustomer != null)
            {
                HttpContext.Session.SetString("NameCustomer", res.vmCustomer.NameCustomer);
                HttpContext.Session.SetInt32("IdCustomer", res.vmCustomer.IdCustomer);
                HttpContext.Session.SetInt32("IdRole", Convert.ToInt32(res.vmCustomer.IdRole));
            }
            return Json(res);
        }

        [HttpGet]
        public ActionResult Logout()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Login");
        }

        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Register(VMCustomers dataParam)
        {
            VMRespon res = Repo.AuthRepo.CreateCustomer(db, dataParam);
            return Json(res);

        }


        public JsonResult CheckEmail(string email)
        {
            VMRespon res = Repo.AuthRepo.EmailCheck(db, email);

            return Json(res);
        }
    }
}
