﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoSaya.DataModels;
using TokoSaya.ViewModels;
using TokoSaya.Web.AddOns;

namespace TokoSaya.Web.Controllers
{
    public class CatalogController : Controller
    {
        private readonly DBfirst_TokoSayaContext db;

        public CatalogController(DBfirst_TokoSayaContext _db)
        {
            this.db = _db;
        }

        public int getIdCustomer()
        {
            return Convert.ToInt32(HttpContext.Session.GetInt32("IdCustomer"));
        }

        public async Task<IActionResult> Index(string sortOrder,
                                              string searchString,
                                              string currentFilter,
                                              int? pageNumber,
                                              int? totalProduct,
                                              int? estimatePrice, int? IdCategory)
        {
            ViewData["NameSortParm"] = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewData["CurrentSort"] = sortOrder;

            ViewBag.TotalProduct = totalProduct > 0 ? totalProduct : 0;
            ViewBag.EstimatePrice = estimatePrice > 0 ? estimatePrice : 0;
            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewData["CurrentFilter"] = searchString;


            List<VMProducts> data = Repo.ProductRepo.AllData(db);

            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a => a.NameProduct.ToLower().Contains(searchString.ToLower()))
                                .ToList();
            }

            switch (sortOrder)
            {
                case "name_desc":
                    data = data.OrderByDescending(a => a.NameProduct).ToList();
                    break;
                default:
                    data = data.OrderBy(a => a.NameProduct).ToList();
                    break;
            }
            int pageSize = 4;
            ViewBag.CategoryList = await Repo.CategoryRepo.AllData(db);

            return View(data);
            //return View(PaginatedList<VMProducts>.CreateAsync(data, pageNumber ?? 1, pageSize));
        }

        [HttpPost]
        public IActionResult OrderPreview(List<VMProducts> list, int TotalProduct, int Estimate)
        {
            ViewBag.TotalProduct = TotalProduct;
            ViewBag.Estimate = Estimate;
            return View(list);
        }

        [HttpPost]
        public IActionResult FixOrder(List<VMProducts> list, int TotalProduct, int Estimate)
        {

            VMRespon res = Repo.OrderRepo.Order(db, list, TotalProduct, Estimate, getIdCustomer());
            if (res.isValid)
            {
                return Json(new { status = "success", data = res });
            }
            return Json(new { status = "failed", data = res });
        }

        public IActionResult History()
        {
            List<VMOrderHeaders> data = Repo.OrderRepo.DataHeader(db, getIdCustomer());
            return View(data);
        }

        public IActionResult Detail(int id)
        {
            List<VMOrderHeaders> data = Repo.OrderRepo.DataDetail(db, id);
            VMOrderHeaders dataObj = data.FirstOrDefault();
            ViewBag.DataObj = dataObj;
            return View(data);
        }
    }
}
