﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoSaya.DataModels;
using TokoSaya.ViewModels;
using TokoSaya.Web.AddOns;

namespace TokoSaya.Web.Controllers
{
    public class CategoryController : Controller
    {
        private readonly DBfirst_TokoSayaContext db;
        public CategoryController(DBfirst_TokoSayaContext _db)
        {
            this.db = _db;
        }


        public async Task<IActionResult> Index(string sortOrder,
                                               string searchString,
                                               string currentFilter,
                                               int? pageNumber)
        {
            ViewData["NameSortParm"] = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewData["CurrentSort"] = sortOrder;


            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewData["CurrentFilter"] = searchString;


            List<VMCategory> data = await Repo.CategoryRepo.AllData(db);

            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a => a.NameCategory.ToLower().Contains(searchString.ToLower())
                                    || a.Description.ToLower().Contains(searchString.ToLower()))
                                .ToList();
            }

            switch (sortOrder)
            {
                case "name_desc":
                    data = data.OrderByDescending(a => a.NameCategory).ToList();
                    break;
                default:
                    data = data.OrderBy(a => a.NameCategory).ToList();
                    break;
            }
            int pageSize = 3;

            return View(PaginatedList<VMCategory>.CreateAsync(data, pageNumber ?? 1, pageSize));
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(VMCategory dataParam)
        {
            int? IdCustomer = HttpContext.Session.GetInt32("IdCustomer");
            dataParam.CreateBy = IdCustomer;

            VMRespon res = Repo.CategoryRepo.Create(db, dataParam);
            if (res.isValid)
            {
                //return RedirectToAction("Index");
                return Json(new { status = "success", data = res });
            }
            return Json(new { status = "failed", data = res });
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            VMCategory data = Repo.CategoryRepo.GetDataById(db, id);
            return View(data);
        }

        [HttpPost]
        public IActionResult Edit(VMCategory dataParam)
        {
            VMRespon res = Repo.CategoryRepo.Edit(db, dataParam);
            if (res.isValid)
            {
                return Json(new { status = "success", data = res });
            }
            return Json(new { status = "failed", data = res });

        }

        [HttpGet]
        public IActionResult Detail(int id)
        {
            VMCategory data = Repo.CategoryRepo.GetDataById(db, id);
            return View(data);
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            VMCategory data = Repo.CategoryRepo.GetDataById(db, id);
            return View(data);
        }

        [HttpPost]
        public IActionResult Delete(VMCategory dataParam)
        {
            VMRespon res = Repo.CategoryRepo.Delete(db, dataParam);
            if (res.isValid)
            {
                return Json(new { status = "success", data = res });

            }
            return Json(new { status = "failed", data = res });
            ;
        }
    }
}
