﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using TokoSaya.Web.Models;

namespace TokoSaya.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult TestRazor()
        {
            string[] data = new string[3] { "Red", "Blue", "Green" };

            List<Friend> listFriend = new List<Friend>()
            {
                new Friend {id=1, name="Abhimanyu", address="Bokaro"},
                new Friend {id=2, name="Deepak", address="Dhanbad"},
                new Friend {id=3, name="Sanjay", address="Patna"},
            };

            Friend dataFriend = new Friend();

            dataFriend.id = 1;
            dataFriend.name = "Kriss";
            dataFriend.address = "Jogja";

            ViewBag.DataColor = data;
            ViewBag.listFriend = listFriend;
            ViewBag.message = "Hallo this is viewbag";
            return View(dataFriend);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
