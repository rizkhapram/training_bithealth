﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TokoSaya.DataModels;
using TokoSaya.ViewModels;
using TokoSaya.Web.AddOns;

namespace TokoSaya.Web.Controllers
{
    public class ProductController : Controller
    {
        private readonly DBfirst_TokoSayaContext db;
        private readonly IWebHostEnvironment webHostEnvironment;
        public ProductController(DBfirst_TokoSayaContext _db, IWebHostEnvironment hostEnvironment)
        {
            this.db = _db;
            this.webHostEnvironment = hostEnvironment;
        }
        public IActionResult Index(string sortOrder,
                                               string searchString,
                                               string currentFilter,
                                               int? pageNumber)
        {
            ViewData["NameSortParm"] = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewData["CurrentSort"] = sortOrder;


            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewData["CurrentFilter"] = searchString;


            List<VMProducts> data = Repo.ProductRepo.AllData(db);

            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a => a.NameProduct.ToLower().Contains(searchString.ToLower()))
                                .ToList();
            }

            switch (sortOrder)
            {
                case "name_desc":
                    data = data.OrderByDescending(a => a.NameProduct).ToList();
                    break;
                default:
                    data = data.OrderBy(a => a.NameProduct).ToList();
                    break;
            }
            int pageSize = 3;

            return View(PaginatedList<VMProducts>.CreateAsync(data, pageNumber ?? 1, pageSize));
        }

        [HttpGet]
        public async Task<IActionResult> Create()
        {
            List<VMVariants> listVariant = new List<VMVariants>();
            List<VMCategory> listCategory = await Repo.CategoryRepo.AllData(db);
            ViewBag.ListCategory = listCategory;
            ViewBag.ListVariant = listVariant;

            VMProducts data = new VMProducts();

            return View(data);
        }

        [HttpPost]
        public IActionResult Create(VMProducts dataParam)
        {
            string uniqueFileName = UploadedFile(dataParam);
            dataParam.Image = uniqueFileName;

            VMRespon res = Repo.ProductRepo.Create(db, dataParam);
            if (res.isValid)
            {
                //return RedirectToAction("Index");
                return Json(new { status = "success", data = res });
            }
            return Json(new { status = "failed", data = res });
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            VMProducts data = Repo.ProductRepo.GetDataById(db, id);


            List<VMVariants> listVariant = Repo.VariantRepo.DataVariantByIdCategory(db, data.IdCategory);
            List<VMCategory> listCategory = await Repo.CategoryRepo.AllData(db);
            ViewBag.ListCategory = listCategory;
            ViewBag.ListVariant = listVariant;

            return View(data);
        }

        [HttpPost]
        public IActionResult Edit(VMProducts dataParam)
        {
            string uniqueFileName = UploadedFile(dataParam);
            dataParam.Image = uniqueFileName;

            VMRespon res = Repo.ProductRepo.Edit(db, dataParam);
            if (res.isValid)
            {
                return Json(new { status = "success", data = res });
            }
            return Json(new { status = "failed", data = res });

        }

        [HttpGet]
        public IActionResult Detail(int id)
        {
            VMProducts data = Repo.ProductRepo.GetDataById(db, id);
            return View(data);
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            VMProducts data = Repo.ProductRepo.GetDataById(db, id);
            return View(data);
        }

        [HttpPost]
        public IActionResult Delete(VMProducts dataParam)
        {
            VMRespon res = Repo.ProductRepo.Delete(db, dataParam);
            if (res.isValid)
            {
                return Json(new { status = "success", data = res });

            }
            return Json(new { status = "failed", data = res });
            
        }

        public JsonResult DataVariantAjax(int id)
        {
            List<VMVariants> list = Repo.VariantRepo.DataVariantByIdCategory(db, id);
            return Json(list);
        }

        private string UploadedFile(VMProducts model)
        {
            string uniqueFileName = null;

            if (model.ImageFile != null)
            {
                string uploadsFolder = Path.Combine(webHostEnvironment.WebRootPath, "images");
                uniqueFileName = Guid.NewGuid().ToString() + "_" + model.ImageFile.FileName;
                string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    model.ImageFile.CopyTo(fileStream);
                }
            }
            return uniqueFileName;
        }
    }
}
