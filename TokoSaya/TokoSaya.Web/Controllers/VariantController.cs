﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoSaya.DataModels;
using TokoSaya.ViewModels;
using TokoSaya.Web.AddOns;

namespace TokoSaya.Web.Controllers
{
    public class VariantController : Controller
    {
        private readonly DBfirst_TokoSayaContext db;
        public VariantController(DBfirst_TokoSayaContext _db)
        {
            this.db = _db;
        }
        public IActionResult Index(string sortOrder,
                                               string searchString,
                                               string currentFilter,
                                               int? pageNumber)
        {
            ViewData["NameSortParm"] = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewData["CurrentSort"] = sortOrder;


            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewData["CurrentFilter"] = searchString;


            List<VMVariants> data = Repo.VariantRepo.AllData(db);

            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a => a.NameVariant.ToLower().Contains(searchString.ToLower())
                                    || a.Description.ToLower().Contains(searchString.ToLower()))
                                .ToList();
            }

            switch (sortOrder)
            {
                case "name_desc":
                    data = data.OrderByDescending(a => a.NameVariant).ToList();
                    break;
                default:
                    data = data.OrderBy(a => a.NameVariant).ToList();
                    break;
            }
            int pageSize = 3;

            return View(PaginatedList<VMVariants>.CreateAsync(data, pageNumber ?? 1, pageSize));
        }

        [HttpGet]
        public async Task<IActionResult> Create()
        {
            List<VMCategory> listCategory = await Repo.CategoryRepo.AllData(db);
            ViewBag.ListCategory = listCategory;
            return View();
        }

        [HttpPost]
        public IActionResult Create(VMVariants dataParam)
        {
            VMRespon res = Repo.VariantRepo.Create(db, dataParam);
            if (res.isValid)
            {
                //return RedirectToAction("Index");
                return Json(new { status = "success", data = res });
            }
            return Json(new { status = "failed", data = res });
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            VMVariants data = Repo.VariantRepo.GetDataById(db, id);
            List<VMCategory> listCategory = await Repo.CategoryRepo.AllData(db);
            ViewBag.ListCategory = listCategory;

            return View(data);
        }

        [HttpPost]
        public IActionResult Edit(VMVariants dataParam)
        {
            VMRespon res = Repo.VariantRepo.Edit(db, dataParam);
            if (res.isValid)
            {
                return Json(new { status = "success", data = res });
            }
            return Json(new { status = "failed", data = res });

        }

        [HttpGet]
        public IActionResult Detail(int id)
        {
            VMVariants data = Repo.VariantRepo.GetDataById(db, id);
            return View(data);
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            VMVariants data = Repo.VariantRepo.GetDataById(db, id);
            return View(data);
        }

        [HttpPost]
        public IActionResult Delete(VMVariants dataParam)
        {
            VMRespon res = Repo.VariantRepo.Delete(db, dataParam);
            if (res.isValid)
            {
                return Json(new { status = "success", data = res });

            }
            return Json(new { status = "failed", data = res });
            ;
        }
    }
}
