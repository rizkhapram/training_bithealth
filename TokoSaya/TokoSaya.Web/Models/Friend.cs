﻿namespace TokoSaya.Web.Models
{
    public class Friend
    {
        public int id { get; set; }

        public string name { get; set; }

        public string address { get; set; }
    }
}
