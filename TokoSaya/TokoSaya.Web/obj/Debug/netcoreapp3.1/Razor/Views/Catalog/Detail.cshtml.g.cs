#pragma checksum "F:\Trainer\Project\.NetCore\BitHealth\TokoSaya\TokoSaya.Web\Views\Catalog\Detail.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "044496cfa7a066972d70b4c399d1330bbd912878"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Catalog_Detail), @"mvc.1.0.view", @"/Views/Catalog/Detail.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "F:\Trainer\Project\.NetCore\BitHealth\TokoSaya\TokoSaya.Web\Views\_ViewImports.cshtml"
using TokoSaya.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "F:\Trainer\Project\.NetCore\BitHealth\TokoSaya\TokoSaya.Web\Views\_ViewImports.cshtml"
using TokoSaya.Web.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"044496cfa7a066972d70b4c399d1330bbd912878", @"/Views/Catalog/Detail.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"dfac90841a6d687eec765b0d2a36e3d33d3ab4ff", @"/Views/_ViewImports.cshtml")]
    public class Views_Catalog_Detail : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<List<TokoSaya.ViewModels.VMOrderHeaders>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/assets/images/logo-icon.png"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("alt", new global::Microsoft.AspNetCore.Html.HtmlString("homepage"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("dark-logo"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 2 "F:\Trainer\Project\.NetCore\BitHealth\TokoSaya\TokoSaya.Web\Views\Catalog\Detail.cshtml"
   Layout = null;
    TokoSaya.ViewModels.VMOrderHeaders data = ViewBag.DataObj;

#line default
#line hidden
#nullable disable
            WriteLiteral("<div class=\"row align-items-center\">\r\n    <div class=\"col-2\">\r\n        <div class=\"form-group\">\r\n            <b class=\"logo-icon\">\r\n                <!-- Dark Logo icon -->\r\n                ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "044496cfa7a066972d70b4c399d1330bbd9128784678", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
            </b>
        </div>
    </div>
    <div class=""col-10"">

        <div class=""form-group"">
            <h2>TokoSaya</h2>
        </div>
    </div>

</div>
<hr />
<div class=""row"">
    <div class=""col-md-6"">
        <div class=""form-group"">
            Code Transaction
        </div>
    </div>
    <div class=""col-md-6"">
        <div class=""form-group"">
            ");
#nullable restore
#line 31 "F:\Trainer\Project\.NetCore\BitHealth\TokoSaya\TokoSaya.Web\Views\Catalog\Detail.cshtml"
       Write(data.CodeTransaction);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<div class=\"row\">\r\n    <div class=\"col-md-6\">\r\n        <div class=\"form-group\">\r\n            Date\r\n        </div>\r\n    </div>\r\n    <div class=\"col-md-6\">\r\n        <div class=\"form-group\">\r\n            ");
#nullable restore
#line 44 "F:\Trainer\Project\.NetCore\BitHealth\TokoSaya\TokoSaya.Web\Views\Catalog\Detail.cshtml"
       Write(data.CreateDate?.ToString("dd MMM yyyy"));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
        </div>
    </div>
</div>

<br>
<table class=""table table-striped"">
    <thead class=""bold"">
        <tr>
            <td>Name Product</td>
            <td>Price</td>
            <td>Qty</td>
            <td>Sum Price</td>
        </tr>
    </thead>
    <tbody>
");
#nullable restore
#line 60 "F:\Trainer\Project\.NetCore\BitHealth\TokoSaya\TokoSaya.Web\Views\Catalog\Detail.cshtml"
         foreach (var item in Model)
        {

#line default
#line hidden
#nullable disable
            WriteLiteral("            <tr>\r\n                <td>");
#nullable restore
#line 63 "F:\Trainer\Project\.NetCore\BitHealth\TokoSaya\TokoSaya.Web\Views\Catalog\Detail.cshtml"
               Write(item.IdProductNavigation.NameProduct);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                <td>");
#nullable restore
#line 64 "F:\Trainer\Project\.NetCore\BitHealth\TokoSaya\TokoSaya.Web\Views\Catalog\Detail.cshtml"
               Write(string.Format("{0:0,0.00}", @item.IdProductNavigation.Price));

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                <td>");
#nullable restore
#line 65 "F:\Trainer\Project\.NetCore\BitHealth\TokoSaya\TokoSaya.Web\Views\Catalog\Detail.cshtml"
               Write(item.IdDetailNavigation.Quantity);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                <td>");
#nullable restore
#line 66 "F:\Trainer\Project\.NetCore\BitHealth\TokoSaya\TokoSaya.Web\Views\Catalog\Detail.cshtml"
               Write(string.Format("{0:0,0.00}", @item.IdDetailNavigation.SumPrice));

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n            </tr>\r\n");
#nullable restore
#line 68 "F:\Trainer\Project\.NetCore\BitHealth\TokoSaya\TokoSaya.Web\Views\Catalog\Detail.cshtml"
        }

#line default
#line hidden
#nullable disable
            WriteLiteral("        <tr class=\"bg-info text-white bold\">\r\n            <td colspan=\"3\">Total</td>\r\n            <td>");
#nullable restore
#line 71 "F:\Trainer\Project\.NetCore\BitHealth\TokoSaya\TokoSaya.Web\Views\Catalog\Detail.cshtml"
           Write(string.Format("{0:0,0.00}", data.Amount));

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n        </tr>\r\n    </tbody>\r\n\r\n</table>\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<List<TokoSaya.ViewModels.VMOrderHeaders>> Html { get; private set; }
    }
}
#pragma warning restore 1591
