#pragma checksum "F:\Trainer\Project\.NetCore\BitHealth\TokoSaya\TokoSaya.Web\Views\Home\TestRazor.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "3e4632ccd1b857147b34fe706bc133233b0148f3"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_TestRazor), @"mvc.1.0.view", @"/Views/Home/TestRazor.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "F:\Trainer\Project\.NetCore\BitHealth\TokoSaya\TokoSaya.Web\Views\_ViewImports.cshtml"
using TokoSaya.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "F:\Trainer\Project\.NetCore\BitHealth\TokoSaya\TokoSaya.Web\Views\_ViewImports.cshtml"
using TokoSaya.Web.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"3e4632ccd1b857147b34fe706bc133233b0148f3", @"/Views/Home/TestRazor.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"dfac90841a6d687eec765b0d2a36e3d33d3ab4ff", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_TestRazor : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<TokoSaya.Web.Models.Friend>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n<h2>");
#nullable restore
#line 3 "F:\Trainer\Project\.NetCore\BitHealth\TokoSaya\TokoSaya.Web\Views\Home\TestRazor.cshtml"
Write(ViewBag.message);

#line default
#line hidden
#nullable disable
            WriteLiteral("</h2>\r\n\r\n<table class=\"table\">\r\n    <thead>\r\n        <tr>\r\n");
#nullable restore
#line 8 "F:\Trainer\Project\.NetCore\BitHealth\TokoSaya\TokoSaya.Web\Views\Home\TestRazor.cshtml"
             for (var i = 1; i <= ViewBag.DataColor.Length; i++)
            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                <td>color ");
#nullable restore
#line 10 "F:\Trainer\Project\.NetCore\BitHealth\TokoSaya\TokoSaya.Web\Views\Home\TestRazor.cshtml"
                     Write(i);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n");
#nullable restore
#line 11 "F:\Trainer\Project\.NetCore\BitHealth\TokoSaya\TokoSaya.Web\Views\Home\TestRazor.cshtml"
            }

#line default
#line hidden
#nullable disable
            WriteLiteral("        </tr>\r\n    </thead>\r\n    <tbody>\r\n        <tr>\r\n");
#nullable restore
#line 16 "F:\Trainer\Project\.NetCore\BitHealth\TokoSaya\TokoSaya.Web\Views\Home\TestRazor.cshtml"
             foreach (var item in ViewBag.DataColor)
            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                <td>Color ");
#nullable restore
#line 18 "F:\Trainer\Project\.NetCore\BitHealth\TokoSaya\TokoSaya.Web\Views\Home\TestRazor.cshtml"
                     Write(item);

#line default
#line hidden
#nullable disable
            WriteLiteral(" </td>\r\n");
#nullable restore
#line 19 "F:\Trainer\Project\.NetCore\BitHealth\TokoSaya\TokoSaya.Web\Views\Home\TestRazor.cshtml"
            }

#line default
#line hidden
#nullable disable
            WriteLiteral("        </tr>\r\n    </tbody>\r\n</table>\r\n\r\n<p>");
#nullable restore
#line 24 "F:\Trainer\Project\.NetCore\BitHealth\TokoSaya\TokoSaya.Web\Views\Home\TestRazor.cshtml"
Write(Model.id);

#line default
#line hidden
#nullable disable
            WriteLiteral("</p>\r\n<p>");
#nullable restore
#line 25 "F:\Trainer\Project\.NetCore\BitHealth\TokoSaya\TokoSaya.Web\Views\Home\TestRazor.cshtml"
Write(Model.name);

#line default
#line hidden
#nullable disable
            WriteLiteral("</p>\r\n<p>");
#nullable restore
#line 26 "F:\Trainer\Project\.NetCore\BitHealth\TokoSaya\TokoSaya.Web\Views\Home\TestRazor.cshtml"
Write(Model.address);

#line default
#line hidden
#nullable disable
            WriteLiteral("</p>\r\n\r\n<h2>Data from return View</h2>\r\n<table class=table>\r\n    <thead>\r\n        <tr>\r\n            <td>Id</td>\r\n            <td>Name</td>\r\n            <td>Address</td>\r\n        </tr>\r\n    </thead>\r\n    <tbody>\r\n");
#nullable restore
#line 38 "F:\Trainer\Project\.NetCore\BitHealth\TokoSaya\TokoSaya.Web\Views\Home\TestRazor.cshtml"
         foreach (var item in  ViewBag.listFriend)
        {

#line default
#line hidden
#nullable disable
            WriteLiteral("            <tr>\r\n                <td>");
#nullable restore
#line 41 "F:\Trainer\Project\.NetCore\BitHealth\TokoSaya\TokoSaya.Web\Views\Home\TestRazor.cshtml"
               Write(item.id);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                <td>");
#nullable restore
#line 42 "F:\Trainer\Project\.NetCore\BitHealth\TokoSaya\TokoSaya.Web\Views\Home\TestRazor.cshtml"
               Write(item.name);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                <td>");
#nullable restore
#line 43 "F:\Trainer\Project\.NetCore\BitHealth\TokoSaya\TokoSaya.Web\Views\Home\TestRazor.cshtml"
               Write(item.address);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n            </tr>\r\n");
#nullable restore
#line 45 "F:\Trainer\Project\.NetCore\BitHealth\TokoSaya\TokoSaya.Web\Views\Home\TestRazor.cshtml"
        }

#line default
#line hidden
#nullable disable
            WriteLiteral("    </tbody>\r\n</table>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<TokoSaya.Web.Models.Friend> Html { get; private set; }
    }
}
#pragma warning restore 1591
